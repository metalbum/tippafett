var app = angular.module('app', ['firebase', 'ngRoute', 'ngAnimate']);
var firebaseRef = new Firebase("https://tippafett.firebaseio.com/");


app.value('autDataHolder', {data:null});

function setGlobalAuthData($scope, $firebaseAuth) {
	$scope.auth = $firebaseAuth(firebaseRef);
	$scope.auth.$onAuth(function(authData) {
    	autDataHolder.data = authData;
    });
}

var teams = [
		"Frölunda",
		"Färjestad",
		"Brynäs",
		"Skellefteå", 
		"HV71",
		"Örebro", 
		"Rögle",
		"Linköping",
		"Malmö",
		"Karlskrona",
		"Djurgården",
		"MODO", 
		"Växjö",
		"Luleå"
	];

// app.animation(".maincontent", [function(){
//     return {
//         enter: function(element, doneFn) {
//             console.log(element);
//             // TweenMax.from(element, 0.5, {css:{scale:0.05, opacity:0}, ease: Elastic.easeInOut.config(1.5, 0.3)});
//             	TweenMax.fromTo(element, 0.3, {x:window.innerWidth}, {x:0,  onComplete:doneFn});
//         },
//         leave: function(element, doneFn) {
// 				TweenMax.fromTo(element, 0.3, {x:0}, {x:-window.innerWidth, ease: Elastic.easeOut.config( 1, 0.3),  onComplete:doneFn});
//         }
//     };
// }]);

app.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/", {
        templateUrl: "login.html"
    });
    $routeProvider.when("/register/", {
        templateUrl: "register.html"
    });
    $routeProvider.when("/edittable/", {
        templateUrl: "edittable.html"
    });
    $routeProvider.when("/tables/", {
        templateUrl: "tables.html"
    });
    $routeProvider.otherwise({
        redirectTo: "/"
    });
}]);

app.controller("mainCtrl", ["$scope", "$timeout", "$location", "$firebaseAuth", "autDataHolder", function($scope, $timeout, $location, $firebaseAuth, autDataHolder){

	// $scope.logIn = function($timeout, $location, $firebaseArray, newUser) {
	// 	firebaseRef.authWithPassword({
	// 		email    : $scope.email,
	// 		password : $scope.password
	// 	}, function(error, authData) {
	// 		if (error) {
	// 			console.log("Login Failed!", error);
	// 			alert(error.message);
	// 		} else {
	// 			console.log("Authenticated successfully with payload:", authData);
	// 			firebaseRef.child("users").once('value', function(value) {
	// 				globalAuthData = authData;
	// 				// console.log(ss.child(authData.uid).val().table);
	// 				// if (newUser) {
	// 				// 	firebaseRef.child("users").child(authData.uid).set("hello");
	// 				// }
	// 			    if(value.child(authData.uid).val()) {
	// 			    	$timeout(function(){
	// 						$location.path("/tables");
	// 					}, 0);
	// 			    } else {
	// 			    	$timeout(function(){
	// 						$location.path("/edittable");
	// 					}, 0);
	// 			    }

	// 			});
	// 		}
	// 	});
	// };

	$scope.logIn = function() {
        if ($scope.loginForm.$valid) {
            firebaseRef.authWithPassword({
                email    : $scope.email,
                password : $scope.password
            }, function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                    alert(error.message);
                } else {
                    console.log("Authenticated successfully with payload:", authData);
                    firebaseRef.child("users").once('value', function(value) {
						autDataHolder.data = authData;
					    if(value.child(authData.uid).val()) {
					    	$timeout(function(){
								$location.path("/tables");
							}, 0);
					    } else {
					    	$timeout(function(){
								$location.path("/edittable");
							}, 0);
					    }
					});   
                }
            });
        } else {
            console.log("Invalid input!");
        }
    };

	$scope.goToRegister = function() {
		$timeout(function(){
			$location.path("/register");
		}, 0);
	};

	$scope.logout = function() {
        firebaseRef.unauth();
        autDataHolder.data = null;
        $location.path("/");
    };

    $scope.auth = $firebaseAuth(firebaseRef);
	$scope.auth.$onAuth(function(authData) {
    	autDataHolder.data = authData;
    });
}]);

app.controller("registerCtrl", ["$scope", "$location", "$timeout", "autDataHolder", function($scope, $location, $timeout, autDataHolder){
	$scope.register = function(event) {

		event.preventDefault();

			if ($scope.registerForm.$valid) {
				firebaseRef.createUser({
					email    : $scope.email,
					password : $scope.password
				}, function(error, userData) {
					if (error) {
				    	console.log("Error creating user:", error);
				    	$location.path("/");
				  	} else {
				    	console.log("Successfully created user account with uid:", userData.uid);
				    	autDataHolder.data = userData;
				    	$timeout(function() {
							$location.path("/edittable"); 
						}, 0);
				  	}
				});
			} else {
				console.log("Invalid input!");
			}
	};
}]);

app.controller("edittableController", ["$scope", "$location", "autDataHolder", function($scope, $location, autDataHolder) {

	$scope.name;
	$scope.teams = [
		{name:"Brynäs", added:false},
		{name:"Djurgården", added:false},
		{name:"Frölunda", added:false},
		{name:"Färjestad", added:false},
		{name:"HV71", added:false},
		{name:"Karlskrona", added:false},
		{name:"Linköping", added:false},
		{name:"Luleå", added:false},
		{name:"Malmö", added:false},
		{name:"MODO", added:false},
		{name:"Rögle", added:false},
		{name:"Skellefteå", added:false},
		{name:"Växjö", added:false},
		{name:"Örebro", added:false}
	];

	$scope.selected = new Array($scope.teams.length);
	$scope.formValid = false;
	var user = {
		name: "",
		table: []
	};

	$scope.submit = function(){
		user.name = $scope.userName;
		if($scope.tableForm.$valid) {
			var selected = angular.copy($scope.selected);
			firebaseRef.child("users").child(autDataHolder.data.uid).set(user);
			firebaseRef.child("users").child(autDataHolder.data.uid).child("table").set(selected);
			$location.path("/tables");
		}
	};

	$scope.insertTeamAtIndex = function(selected, atIndex){
		$scope.selected[atIndex] = selected.name;
		$scope.checkIfValid();
	};

	$scope.checkIfValid = function(){
		for (var i = 0; i < $scope.teams.length; i++) {
			if ($scope.selected.indexOf($scope.teams[i].name) < 0) {
				$scope.formValid = false;
				return;
			}
		}
		$scope.formValid = true;
	};

}]);

app.controller("tablesController", ["$scope", "$firebaseArray", "autDataHolder", function($scope, $firebaseArray, autDataHolder) {
	$scope.user = autDataHolder.data.uid;
	$scope.users = $firebaseArray(firebaseRef.child("users"));
	$scope.users.$loaded().then(function() {
		for (var i = 0; i < $scope.users.length; i++) {
			$scope.getTotalScore($scope.users[i].table);
		}
	});

	$scope.getRank = function(team) {

		for (var i = 0; i < teams.length; i++) {
			if (team === teams[i]) {
				return i+1;
			}
		}
	};

	$scope.getScore = function(team, $index) {
		for (var i = 0; i < teams.length; i++) {
			if (team.toLowerCase() === teams[i].toLowerCase() && $index === i) {
				return 3;
			} 
			if (i !== 0) {
				if (team.toLowerCase() === teams[i].toLowerCase() && $index === i-1) {
					return 1;
				}
			}
			if (i !== 13) {
				if (team.toLowerCase() === teams[i].toLowerCase() && $index === i+1) {
					return 1;
				}
			}
		}
		return 0;
	};

	$scope.getTotalScore = function(table) {
		var threes = 0;
		var ones = 0;
		var zeros = 0;
		totalScore = 0;
		for (var i = 0; i < table.length; i++) {
			var score = $scope.getScore(table[i], i);
			if (score === 3) {
				threes++;
			} if (score === 1) {
				ones++;
			} else {
				zeros++;
			}
			totalScore += score; 
		}
		return totalScore;
	};

	$scope.getImageSrc = function(team) {
		return "img/" + team.toLowerCase().replace("å", "_").replace("ä", "_").replace("ö", "_") + ".png";
	}
}]);


















