app.controller("mainCtrl", ["$scope", "$timeout", "$location", "$firebaseAuth", "authDataHolder",
					function($scope, $timeout, $location, $firebaseAuth, authDataHolder) {

	$scope.loggedIn = false;

	$scope.logIn = function() {
        if ($scope.loginForm.$valid) {
            firebaseRef.authWithPassword({
                email    : $scope.email,
                password : $scope.password
            }, function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                    alert(error.message);
                } else {
                    console.log("Authenticated successfully with payload:", authData);
                    firebaseRef.child("users").once('value', function(value) {
						authDataHolder.data = authData;
						$scope.loggedIn = true;
					    if(value.child(authData.uid).val()) {
					    	$timeout(function(){
								$location.path("/tables");
							}, 0);
					    } else {
					    	$timeout(function(){
								$location.path("/edittable");
							}, 0);
					    }
					});   
                }
            });
        } else {
            console.log("Invalid input!");
        }
    };

	$scope.goToRegister = function() {
		$timeout(function(){
			$location.path("/register");
		}, 0);
	};

	$scope.logout = function() {
        firebaseRef.unauth();
        authDataHolder.data = null;
        $scope.loggedIn = false;
        $location.path("/");
    };

    console.log(authDataHolder.data == null);

    $scope.auth = $firebaseAuth(firebaseRef);
	$scope.auth.$onAuth(function(authData) {
    	authDataHolder.data = authData;
    });

}]);

app.controller("registerCtrl", ["$scope", "$location", "$timeout", "authDataHolder", function($scope, $location, $timeout, authDataHolder){
	$scope.register = function(event) {

		event.preventDefault();

			if ($scope.registerForm.$valid) {
				firebaseRef.createUser({
					email    : $scope.email,
					password : $scope.password
				}, function(error, userData) {
					if (error) {
				    	console.log("Error creating user:", error);
				    	$location.path("/");
				  	} else {
				    	console.log("Successfully created user account with uid:", userData.uid);
				    	authDataHolder.data = userData;
				    	$timeout(function() {
							$location.path("/edittable"); 
						}, 0);
				  	}
				});
			} else {
				console.log("Invalid input!");
			}
	};
}]);



// app.animation(".maincontent", [function(){
//     return {
//         enter: function(element, doneFn) {
//             console.log(element);
//             // TweenMax.from(element, 0.5, {css:{scale:0.05, opacity:0}, ease: Elastic.easeInOut.config(1.5, 0.3)});
//             	TweenMax.fromTo(element, 0.3, {x:window.innerWidth}, {x:0,  onComplete:doneFn});
//         },
//         leave: function(element, doneFn) {
// 				TweenMax.fromTo(element, 0.3, {x:0}, {x:-window.innerWidth, ease: Elastic.easeOut.config( 1, 0.3),  onComplete:doneFn});
//         }
//     };
// }]);


















