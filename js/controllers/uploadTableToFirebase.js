app.controller("uploadTableToFirebaseCtrl", ["$scope", "$location", "$http", "authDataHolder", "$firebaseArray", function($scope, $location, $http, authDataHolder, $teams) {

	var season = "2015-16";
	var url = 'http://127.0.0.1:3000';

	$http.post('http://127.0.0.1:3000/fetchtoken').then(function successCallback(response){
		console.log("Success!", response);
		$scope.accessToken = response.data;
	}, function errorCallback(response){
		console.log("Error!", response)
	});

	// 2016-03-05T11:41:31.734Z: Invalid access token (expired).

	$scope.fetchTable = function() {
		$http.post(url + "/fetchtable", $scope.accessToken).then(function successCallback(response){
			var teamData = formatTeamData(response.data);
			firebaseRef.child("standings").child(season).set(teamData);
		}, function errorCallback(response){
			console.log("Error!", response);
		});
	};

	$scope.getTeamsFromFirebase = function() {
		$scope.teams = $teams(firebaseRef.child("standings").child(season));
		$scope.teams.$loaded().then(function() {
			if ($scope.teams.length < 14) {
				$scope.teams = teams;
			}
		});
	};


	// $scope.selected = new Array($scope.teams.length);
	// $scope.formValid = false;

	$scope.submit = function(){
		if($scope.tableForm.$valid) {
			console.log("VALID");
			var selected = angular.copy($scope.selected);
			console.log(selected);
			firebaseRef.child("standings").child(season).set(selected);
		}
	};

	$scope.insertTeamAtIndex = function(selected, atIndex){
		$scope.selected[atIndex] = selected.$value;
		$scope.checkIfValid();
	};

	$scope.checkIfValid = function(){
		for (var i = 0; i < $scope.teams.length; i++) {
			if ($scope.selected.indexOf($scope.teams[i].$value) < 0) {
				$scope.formValid = false;
				return;
			}
		}
		$scope.formValid = true;
	};


	function formatTeamData(rawData) {

		var teams = [];
		for (var index in rawData) {
				var team = rawData[index].team_code;
				switch(team) {
				    case "BIF":
				        teams.push("Brynäs");
				        break;
				    case "SAIK":
				        teams.push("Skellefteå");
				        break;
				    case "FHC":
				        teams.push("Frölunda");
				        break;
				    case "FBK":
				        teams.push("Färjestad");
				        break;
				    case "LHF":
				        teams.push("Luleå");
				        break;
				    case "DIF":
				        teams.push("Djurgården");
				        break;
				    case "LHC":
				        teams.push("Linköping");
				        break;
				    case "MIF":
				        teams.push("Malmö");
				        break;
				    case "RBK":
				        teams.push("Rögle");
				        break;
				    case "MODO":
				        teams.push("MODO");
				        break;
				    case "VLH":
				        teams.push("Växjö");
				        break;
				    case "KHK":
				        teams.push("Karlskrona");
				        break;
				    case "HV71":
				        teams.push("HV71");
				        break;
				    case "OHK":
				        teams.push("Örebro");
				        break;
				} 
		}
		return teams;
	};

	$scope.getImageSrc = function(team) {
		return "img/" + team.toLowerCase().replace("å", "_").replace("ä", "_").replace("ö", "_") + ".png";
	}

	$scope.getTeamsFromFirebase();
	
}]);