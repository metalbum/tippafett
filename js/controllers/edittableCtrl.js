app.controller("edittableCtrl", ["$scope", "$location", "authDataHolder", function($scope, $location, authDataHolder) {

	$scope.name;
	$scope.teams = [
		{name:"Brynäs", added:false},
		{name:"Djurgården", added:false},
		{name:"Frölunda", added:false},
		{name:"Färjestad", added:false},
		{name:"HV71", added:false},
		{name:"Karlskrona", added:false},
		{name:"Linköping", added:false},
		{name:"Luleå", added:false},
		{name:"Malmö", added:false},
		{name:"MODO", added:false},
		{name:"Rögle", added:false},
		{name:"Skellefteå", added:false},
		{name:"Växjö", added:false},
		{name:"Örebro", added:false}
	];

	$scope.selected = new Array($scope.teams.length);
	$scope.formValid = false;
	var user = {
		name: "",
		table: []
	};

	$scope.submit = function(){
		user.name = $scope.userName;
		if($scope.tableForm.$valid) {
			var selected = angular.copy($scope.selected);
			firebaseRef.child("users").child(authDataHolder.data.uid).set(user);
			firebaseRef.child("users").child(authDataHolder.data.uid).child("table").set(selected);
			$location.path("/tables");
		}
	};

	$scope.insertTeamAtIndex = function(selected, atIndex){
		$scope.selected[atIndex] = selected.name;
		$scope.checkIfValid();
	};

	$scope.checkIfValid = function(){
		for (var i = 0; i < $scope.teams.length; i++) {
			if ($scope.selected.indexOf($scope.teams[i].name) < 0) {
				$scope.formValid = false;
				return;
			}
		}
		$scope.formValid = true;
	};

}]);