app.controller("tablesCtrl", ["$scope", "$firebaseArray", "authDataHolder", "$firebaseArray", function($scope, $users, authDataHolder, $standings) {
	var season = "2015-16"
	$scope.user = authDataHolder.data.uid;
	$scope.standings = $standings(firebaseRef.child("standings").child(season));
	$scope.standings.$loaded().then(function() {
		console.log($scope.standings);
		$scope.users = $users(firebaseRef.child("users"));
		$scope.users.$loaded().then(function() {
			for (var i = 0; i < $scope.users.length; i++) {
				$scope.getTotalScore($scope.users[i].table);
			}
		});
	});

	$scope.getRank = function(team) {

		for (var i = 0; i < $scope.standings.length; i++) {
			if (team === $scope.standings[i].$value) {
				return i+1;
			}
		}
	};

	$scope.getScore = function(team, $index) {
		for (var i = 0; i < $scope.standings.length; i++) {
			if (team.toLowerCase() === $scope.standings[i].$value.toLowerCase() && $index === i) {
				return 3;
			} 
			if (i !== 0) {
				if (team.toLowerCase() === $scope.standings[i].$value.toLowerCase() && $index === i-1) {
					return 1;
				}
			}
			if (i !== 13) {
				if (team.toLowerCase() === $scope.standings[i].$value.toLowerCase() && $index === i+1) {
					return 1;
				}
			}
		}
		return 0;
	};

	$scope.getTotalScore = function(table) {
		var threes = 0;
		var ones = 0;
		var zeros = 0;
		totalScore = 0;
		for (var i = 0; i < table.length; i++) {
			var score = $scope.getScore(table[i], i);
			if (score === 3) {
				threes++;
			} if (score === 1) {
				ones++;
			} else {
				zeros++;
			}
			totalScore += score; 
		}
		return totalScore;
	};

	$scope.getImageSrc = function(team) {
		return "img/" + team.toLowerCase().replace("å", "_").replace("ä", "_").replace("ö", "_") + ".png";
	}
}]);