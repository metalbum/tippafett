var app = angular.module('app', ['firebase', 'ngRoute', 'ngAnimate']);
var firebaseRef = new Firebase("https://tippafett.firebaseio.com/");


app.value('authDataHolder', {data:null});

function setGlobalAuthData($scope, $firebaseAuth) {
	$scope.auth = $firebaseAuth(firebaseRef);
	$scope.auth.$onAuth(function(authData) {
    	autDataHolder.data = authData;
    });
}

var teams = [
		"Frölunda",
		"Brynäs",
		"Linköping",
		"Färjestad", 
		"Skellefteå",
		"Örebro", 
		"Djurgården",
		"Växjö",
		"Rögle",
		"Luleå",
		"Malmö",
		"MODO", 
		"HV71",
		"Karlskrona"
	];