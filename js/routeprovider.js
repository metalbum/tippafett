app.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/", {
        templateUrl: "login.html"
    });
    $routeProvider.when("/register/", {
        templateUrl: "register.html"
    });
    $routeProvider.when("/edittable/", {
        templateUrl: "edittable.html"
    });
    $routeProvider.when("/tables/", {
        templateUrl: "tables.html"
    });
    $routeProvider.otherwise({
        redirectTo: "/"
    });
}]);